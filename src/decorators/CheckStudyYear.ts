import { IValidatorConfig } from "../interfaces/index.js";

const registeredValidators: IValidatorConfig = {};

export function CheckStudyYear(target: any, propName: string) {
  registeredValidators[target.constructor.name] = {
    ...registeredValidators[target.constructor.name],
    [propName]: ["isFiveYearsStudy"],
  };
}

export function Validate(obj: any) {
  const objValidatorConfig = registeredValidators[obj.constructor.name];
  if (!objValidatorConfig) {
    return true;
  }

  let isValid = true;
  for (const prop in objValidatorConfig) {
    for (const validator of objValidatorConfig[prop]) {
      switch (validator) {
        case "isFiveYearsStudy":
          isValid = isValid && obj[prop] >= 1 && obj[prop] <= 5;
          break;
      }
    }
  }
  return isValid;
}
