import { Student } from "../classes/index.js";

export function CheckStudent(target: any, name: string, position: number) {
  if (!target[name].validation) {
    target[name].validation = {};
  }

  Object.assign(target[name].validation, {
    [position]: (s: Student): Student | void => {
      if (s.averageSchoolMark > 3) {
        if (s.age > 17) {
          return s;
        } else {
          throw new Error(`Sorry ${s.name}, your age is under 18 years old`);
        }
      }
      throw new Error(`${s.name}, your average mark is too low`);
    },
  });
}
