import { Student } from "../classes/index.js";

export function handleError(
  func: Function,
  value: Student,
  option?: { [x: string]: unknown }
) {
  try {
    if (option) {
      func(value, option);
    } else {
      func(value);
    }
  } catch (err) {
    console.log(err);
  }
}
