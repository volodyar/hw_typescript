import { Student } from "../classes/index.js";
import { Validate } from "../decorators/index.js";

export function validateStudyYear(obj: Student) {
  if (!Validate(obj)) {
    throw new Error(`${obj.name} you has incorrect year of study!`);
  }
}
