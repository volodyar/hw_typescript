export interface IValidatorConfig {
  [property: string]: {
    [validatableProp: string]: string[];
  };
}
