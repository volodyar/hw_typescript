export interface IStudent {
  _averageMark: number;
  _averageSchoolMark: number;
  studyYear: number;

  getTasks(): number[];
  setTask(task: number): void;
  makeTask(): number;
  clearTasks(): void;
}
