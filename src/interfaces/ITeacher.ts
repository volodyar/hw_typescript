import { University } from "../classes/index.js";
import { unionEmployee } from "../customTypes/unionEmployee.js";

export interface ITeacher {
  readonly department: string;
  readonly academicRank: string;

  setMark(task: number): number;
  checkStudentsTasks(university: University<unionEmployee>): void;
}
