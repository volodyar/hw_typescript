import { Role } from "../enum/Role.js";

export interface IEmployee {
  readonly role: Role;

  setSalary(salary: number): void;
  getSalary(): number;
}
