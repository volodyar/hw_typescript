export { IPerson } from "./IPerson.js";
export { IEmployee } from "./IEmployee.js";
export { ITeacher } from "./ITeacher.js";
export { IValidatorConfig } from "./IValidationConfig.js";
export { IStudent } from "./IStudent.js";
