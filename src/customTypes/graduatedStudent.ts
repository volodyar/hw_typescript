import { IPerson } from "../interfaces/IPerson.js";
import { IStudent } from "../interfaces/IStudent.js";

export type graduatedStudent = IPerson &
  Pick<IStudent, "_averageMark" | "_averageSchoolMark"> & { degree: string };
