import {
  Accounting,
  Rector,
  Security,
  Student,
  Teacher,
  University,
} from "./classes/index.js";
import { unionEmployee } from "./customTypes/unionEmployee.js";
import { Department, AcademicRanks } from "./enum/index.js";
import { handleError, validateStudyYear } from "./utils/index.js";

// Code implementation
const accounting = new Accounting();
const university = new University<unionEmployee>(
  "The Precarpathian National University",
  accounting
);

console.log("University name:", university.name);

// Creation stuff
const rector = new Rector("John", 41, Department.Finance);

const financeTeacher = new Teacher(
  "Katarina",
  29,
  Department.Finance,
  AcademicRanks.AssistantProfessor
);
const psycholodyTeacher = new Teacher(
  "Sara",
  34,
  Department.Psychology,
  AcademicRanks.AssociateProfessor
);
const pedagogyTeacher = new Teacher(
  "Sam",
  50,
  Department.Pedagogy,
  AcademicRanks.Professor
);
const computerScienceTeacher = new Teacher(
  "Adam",
  37,
  Department.ComputerScience,
  AcademicRanks.AssociateProfessor
);

const firstSecurity = new Security("Mike", 40);
const SecondSecurity = new Security("Viking", 50);

// Employment employee
university.addEmployee(rector, true);

university.addEmployee(psycholodyTeacher, true);
university.addEmployee(financeTeacher, true);
university.addEmployee(pedagogyTeacher, true);
university.addEmployee(computerScienceTeacher, true);

university.addEmployee(firstSecurity, true);
university.addEmployee(SecondSecurity, true);

console.log("Employees from the beginning 🧔🧔🧔‍♀️");
console.log(university.employees);

// Fire some employee or give them second chance, it depends on rector decision
university.removeEmployee(firstSecurity, rector.vacancyDeсision());

console.log("Employees without fired one 🔥🔥🔥");
console.log(university.employees);

// Create students
const psycholodyStudent1 = new Student(
  "Mana",
  19,
  university.name,
  Department.Psychology,
  1
);
const psycholodyStudent2 = new Student(
  "Tana",
  18,
  university.name,
  Department.Psychology,
  5
);
const psycholodyStudent3 = new Student(
  "Lama",
  20,
  university.name,
  Department.Psychology,
  5
);

const financeStudent1 = new Student(
  "Tara",
  19,
  university.name,
  Department.Finance,
  5
);
const financeStudent2 = new Student(
  "Sata",
  19,
  university.name,
  Department.Finance,
  2
);
const financeStudent3 = new Student(
  "Kara",
  21,
  university.name,
  Department.Finance,
  6
);

const pedagogyStudent1 = new Student(
  "Ana",
  20,
  university.name,
  Department.Pedagogy,
  5
);
const pedagogyStudent2 = new Student(
  "Katya",
  18,
  university.name,
  Department.Pedagogy,
  5
);

const computerScienceStudent1 = new Student(
  "Mana",
  25,
  university.name,
  Department.ComputerScience,
  1
);
const computerScienceStudent2 = new Student(
  "Tana",
  18,
  "Student of aonther university",
  Department.ComputerScience,
  -1
);
const computerScienceStudent3 = new Student(
  "Lama",
  19,
  university.name,
  Department.ComputerScience,
  5
);
const computerScienceStudent4 = new Student(
  "Saka",
  23,
  university.name,
  Department.ComputerScience,
  5
);

// Students' entry selection. If their average mark from school is less than 4, an error will occur.
console.log("Check if students have an average mark higher than 4 ❓❓❓");
handleError(university.acceptStudentBySchoolMark, psycholodyStudent1);
handleError(university.acceptStudentBySchoolMark, psycholodyStudent2);
handleError(university.acceptStudentBySchoolMark, psycholodyStudent3);

handleError(university.acceptStudentBySchoolMark, pedagogyStudent1);
handleError(university.acceptStudentBySchoolMark, pedagogyStudent2);
handleError(university.acceptStudentBySchoolMark, financeStudent1);
handleError(university.acceptStudentBySchoolMark, financeStudent2);
handleError(university.acceptStudentBySchoolMark, financeStudent3);

handleError(university.acceptStudentBySchoolMark, computerScienceStudent1);
handleError(university.acceptStudentBySchoolMark, computerScienceStudent2);
handleError(university.acceptStudentBySchoolMark, computerScienceStudent3);
handleError(university.acceptStudentBySchoolMark, computerScienceStudent4);

console.log("Students who succeded selection 🎉🎉🎉");
console.log(university.students);

//Check if students have the correct study year
console.log("Check if students have the correct study year ❓❓❓");
university.students.forEach((students) => {
  handleError(validateStudyYear, students);
});

//Check students' pass
console.log("Check if students have the correct pass ❓❓❓");
university.students.forEach((students) => {
  handleError(firstSecurity.checkStudentPass, students, {
    university: university.name,
  });
});

// Count salary for teachers
university.setSalary();
console.log("Employees with counted salary 🤑🤑🤑");
console.log(university.employees);

// Students make thier tasks
for (let i = 0; i <= 8; i += 1) {
  university.students.forEach((student) => student.setTask(student.makeTask()));
}

// Check students' tasks
pedagogyTeacher.checkStudentsTasks(university);
financeTeacher.checkStudentsTasks(university);
psycholodyTeacher.checkStudentsTasks(university);
computerScienceTeacher.checkStudentsTasks(university);

console.log("Students with checked tasks ✅✅✅");
console.log(university.students);

// Graduate students
university.graduateStudents();

// Graduated students and who last for second year
console.log("Graduated Students 🎓🎓🎓");
console.log(university.graduatedStudents);
console.log("Didn't graduate ones ☠️☠️☠️ or who isn't the last year of study");
console.log(university.students);
