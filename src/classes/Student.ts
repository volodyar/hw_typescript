import { LogClassName, CheckStudyYear } from "../decorators/index.js";
import { Department } from "../enum/index.js";
import { IPerson, IStudent } from "../interfaces/index.js";

@LogClassName
export class Student implements IPerson, IStudent {
  private _completedTasks: number[] = [];
  public _averageSchoolMark = 0;
  public _averageMark: number = 0;
  private _checkedTasks: number[] = [];
  @CheckStudyYear
  public studyYear: number;

  constructor(
    public readonly name: string,
    public readonly age: number,
    public readonly university: string,
    public readonly department: Department,
    studyYear: number
  ) {
    this.studyYear = studyYear;
    this._averageSchoolMark = this._setAverageSchoolMark();
  }

  public getTasks() {
    const { _completedTasks } = this;
    return _completedTasks;
  }

  public setTask(task: number): void {
    this._completedTasks.push(task);
  }

  get averageMark(): number {
    return this._averageMark;
  }

  get averageSchoolMark(): number {
    return this._averageSchoolMark;
  }

  public getCheckedTasks(): number[] {
    const { _checkedTasks } = this;

    return _checkedTasks;
  }

  public setCheckedTasks(array: number[]): void {
    this._checkedTasks = array;
    this._setAverageMark();
  }

  public makeTask(): number {
    const max = 50;
    const min = 0;
    const result = Math.round(Math.random() * (max - min + 1) + min);

    return result;
  }

  public clearTasks(): void {
    this._completedTasks = [];
  }

  private _setAverageSchoolMark(): number {
    const max = 12;
    const min = 1;
    const result = Math.round(Math.random() * (max - min + 1) + min);

    return result;
  }

  private _setAverageMark(): void {
    const marks = this._checkedTasks;
    const summedMarks = marks.reduce((acc, el) => acc + el, 0);
    this._averageMark = Math.floor(summedMarks / marks.length);
  }
}
