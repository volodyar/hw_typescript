import { University, Employee } from "./index.js";
import { Department, Role } from "../enum/index.js";
import { unionEmployee } from "../customTypes/unionEmployee.js";
import { ITeacher } from "../interfaces/index.js";
import { LogClassName } from "../decorators/index.js";

@LogClassName
export class Teacher extends Employee implements ITeacher {
  constructor(
    name: string,
    age: number,
    public readonly department: Department,
    public readonly academicRank: string,
    role = Role.TEACHER
  ) {
    super(name, age, role);
  }

  public setMark(task: number): number {
    const max = 50;
    const min = 0;
    const result = Math.round(Math.random() * (max - min + 1) + min + task);

    return result;
  }

  public checkStudentsTasks(university: University<unionEmployee>): void {
    const students = university.students.filter(
      (student) => student.department === this.department
    );

    students.forEach((student) => {
      const tasks = student.getTasks();
      const checkedTasks = tasks.map((task) => this.setMark(task));
      student.clearTasks();

      return student.setCheckedTasks(checkedTasks);
    });
  }
}
