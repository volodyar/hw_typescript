import { unionEmployee } from "../customTypes/unionEmployee.js";
import { LogClassName } from "../decorators/LogClassName.js";
import { Accounting, Student, Employee } from "./index.js";
import { Validation, CheckStudent } from "../decorators/index.js";
import { graduatedStudent } from "../customTypes/graduatedStudent.js";

@LogClassName
export class University<T extends unionEmployee> {
  private _employees: T[] = [];
  private _students: Student[] = [];
  private _graduatedStudents: graduatedStudent[] = [];

  constructor(public readonly name: string, private _department: Accounting) {}

  get employees(): T[] {
    return this._employees;
  }

  get students(): Student[] {
    return this._students;
  }

  get graduatedStudents(): graduatedStudent[] {
    return this._graduatedStudents;
  }

  public setSalary(): void {
    const employees = this._employees;

    this._employees = employees.map((empl) => {
      const salary = this._department.countSalary();
      const role = empl.role;
      empl.setSalary(salary[role]);

      return empl as T;
    });
  }

  // @@@@ check students averageSchoolMark
  @Validation
  public acceptStudentBySchoolMark(@CheckStudent student: Student): void {
    this._students.push(student);
  }

  public addEmployee(employee: T, decision: boolean) {
		if (employee instanceof Employee && decision) {
      this._employees.push(employee);
    }

    if (!(employee instanceof Employee)) {
      throw new Error("Invalid instance of class");
    }

    if (!decision) {
      throw new Error("Sorry, you don't match to our team!");
    }
  }

  public removeEmployee(employee: T, decision: boolean): void {
    if (decision) {
      const employees = this._employees;
      this._employees = employees.filter((empl) => employee.name !== empl.name);
    } else {
      console.log(`WARNING: ${employee.name}, you have the last chance ⚠️⚠️⚠️`);
    }
  }

  public graduateStudents(): void {
    const students = this._students;
    const graduatedStudents = students
      .filter(
        ({ _averageMark, studyYear }) => _averageMark >= 50 && studyYear === 5
      )
      .map((student) => {
        const { name, age, _averageMark, _averageSchoolMark } = student;
        const degree = this.handleDegreeBasedOnMarks(_averageMark);
        const newStudent: graduatedStudent = {
          name,
          age,
          degree,
          _averageMark,
          _averageSchoolMark,
        };

        return newStudent;
      });

    this._graduatedStudents = graduatedStudents;
    this._students = students.filter(({ _averageMark }) => _averageMark < 50);
  }

  private handleDegreeBasedOnMarks(mark: number): string {
    if (mark <= 100 && mark >= 90) {
      return "Excellent degree";
    }

    if (mark <= 89 && mark >= 70) {
      return "Very good degree";
    }

    if (mark <= 69 && mark >= 50) {
      return "Good degree";
    }

    return "Didn't graduated";
  }
}
