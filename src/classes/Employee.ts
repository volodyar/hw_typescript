import { Role } from "../enum/index.js";
import { IEmployee, IPerson } from "../interfaces/index.js";

export abstract class Employee implements IEmployee, IPerson {
  private _salary: number = 0;

  constructor(
    public readonly name: string,
    public readonly age: number,
    public readonly role: Role
  ) {}

  setSalary(s: number): void {
    this._salary = s;
	}
	
  getSalary(): number {
    return this._salary;
  }
}
