import { AcademicRanks, Department, Role } from "../enum/index.js";
import { Teacher } from "./index.js";

export class Rector extends Teacher {
  constructor(name: string, age: number, department: Department) {
    super(name, age, department, AcademicRanks.Professor, Role.DEAN);
  }

  public vacancyDeсision(): boolean {
    const max = 1;
    const min = 0;
    const result = Math.round(Math.random() * (max - min + 1) + min);

    return Boolean(result);
  }
}
