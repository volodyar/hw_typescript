import { Employee, Student } from "./index.js";
import { Role } from "../enum/index.js";

export class Security extends Employee {
  constructor(name: string, age: number) {
    super(name, age, Role.SECURITY);
  }

  checkStudentPass(s: Student, u: { university: string }) {
    if (s.university !== u.university) {
      throw new Error(`${s.name}, you don't belong to this university!`);
    }
  }
}
