import { Role } from "../enum/index.js";

export class Accounting {
  constructor() {}

  public countSalary(): { [x: string]: number } {
    return {
      [Role.DEAN]: 4500,
      [Role.CLEANER]: 2000,
      [Role.SECURITY]: 2700,
      [Role.TEACHER]: 3500,
    };
  }
}
