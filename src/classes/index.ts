export { Employee } from "./Employee.js";
export { Teacher } from "./Teacher.js";
export { Rector } from "./Rector.js";
export { Security } from "./Security.js";
export { Student } from "./Student.js";
export { Accounting } from "./Accounting.js";
export { University } from "./University.js";
