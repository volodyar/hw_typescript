export enum AcademicRanks {
  AssistantProfessor = "assistantProfessor",
  AssociateProfessor = "associateProfessor",
  Professor = "professor",
}
