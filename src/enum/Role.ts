export enum Role {
  TEACHER = "teacher",
  SECURITY = "security",
  DEAN = "dean",
  CLEANER = "cleaner",
}
